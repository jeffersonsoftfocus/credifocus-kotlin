package br.com.softfocus.credifocus.mvp.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonValue
import javax.persistence.*

@Entity
@Table(name = "credito_aprovacaoautomatica_produtos_de_credito")
class AprovacaoAutomaticaProdutosCredito {

    @Id
    @Column(name = "id")
    var id : Int? = null

    @ManyToOne
    @JoinColumn(name = "aprovacaoautomatica_id", referencedColumnName = "id")
    var aprovacaoautomatica: AprovacaoAutomatica? = null

    @ManyToOne
    @JoinColumn(name = "produtocredito_id", referencedColumnName = "id")
    var produtocredito: ProdutoCredito? = null

    @JsonValue
    fun produtos_credito(): ProdutoCredito? {
        return this.produtocredito
    }
}