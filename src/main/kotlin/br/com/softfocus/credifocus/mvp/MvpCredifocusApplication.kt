package br.com.softfocus.credifocus.mvp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.util.*

@SpringBootApplication
open class MvpCredifocusApplication {

	fun init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
	}
}

fun main(args: Array<String>) {
	runApplication<MvpCredifocusApplication>(*args)
}
