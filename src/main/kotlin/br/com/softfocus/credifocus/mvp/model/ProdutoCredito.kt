package br.com.softfocus.credifocus.mvp.model

import java.sql.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "credito_produtocredito")
class ProdutoCredito {

    @Id
    @Column(name = "id")
    var id : Int? = null

    @Column(name = "nome")
    var nome : String? = null
/*
    @Column(name = "valor_maximo_liberado")
    var valor_maximo_liberado : Double? = null

    @Column(name = "prazo_maximo")
    var prazo_maximo : Int? = null

    @Column(name = "prazo_minimo_carencia")
    var prazo_minimo_carencia : Int? = null

    @Column(name = "prazo_maximo_carencia")
    var prazo_maximo_carencia : Int? = null

    @Column(name = "taxa")
    var taxa : Double? = null

    @Column(name = "taxa_agente")
    var taxa_agente : Double? = null

    @Column(name = "custo_financeiro_id")
    var custo_financeiro_id : Int? = null

    @Column(name = "ativo")
    var ativo : Boolean? = null*/
}