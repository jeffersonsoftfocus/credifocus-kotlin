package br.com.softfocus.credifocus.mvp.repository

import br.com.softfocus.credifocus.mvp.model.AprovacaoAutomatica
import org.springframework.data.jpa.repository.JpaRepository

interface AprovacaoAutomaticaRepository : JpaRepository<AprovacaoAutomatica, Long>