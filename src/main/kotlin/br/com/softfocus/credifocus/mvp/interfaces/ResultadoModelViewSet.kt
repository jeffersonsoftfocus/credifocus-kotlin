package br.com.softfocus.credifocus.mvp.interfaces

class ResultadoModelViewSet {
    var count: Int? = null
    var next: String? = null
    var previous: String? = null
    var results: MutableList<Any>? = null
}