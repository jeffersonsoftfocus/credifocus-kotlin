package br.com.softfocus.credifocus.mvp.model

import java.sql.Date
import javax.persistence.*

@Entity
@Table(name = "credito_aprovacaoautomatica")
class AprovacaoAutomatica {

    @Id
    @Column(name = "id")
    var id : Int? = null
    @Column(name = "ativo")
    var ativo : Boolean = false
    @Column(name = "inicio_vigencia")
    var inicio_vigencia : Date? = null
    @Column(name = "final_vigencia")
    var final_vigencia : Date? = null
    @Column(name = "cooperativa_id")
    var cooperativa_id : Int? = null

    @ManyToOne
    @JoinColumn(name = "supervisor_id", referencedColumnName = "id")
    var supervisor : CadastrosSupervisor? = null

    @Column(name = "nome")
    var nome : String? = null
    @Column(name = "nivel_risco_maximo")
    var nivel_risco_maximo : String? = null
    @Column(name = "permite_pessoa_fisica")
    var permite_pessoa_fisica : Boolean? = null
    @Column(name = "permite_pessoa_juridica")
    var permite_pessoa_juridica : Boolean? = null
    @Column(name = "score_risco_minimo")
    var score_risco_minimo : Int? = null
    @Column(name = "valor_maximo")
    var valor_maximo : Double? = null
    @Column(name = "maximo_beneficiario")
    var maximo_beneficiario : Double? = null
    @Column(name = "maximo_cooperativa")
    var maximo_cooperativa : Double? = null
    @Column(name = "maximo_produto_credito")
    var maximo_produto_credito : Double? = null
    @Column(name = "situacao_financeira_pessoa_fisica_id")
    var situacao_financeira_pessoa_fisica_id : Int? = null
    @Column(name = "situacao_financeira_pessoa_juridica_id")
    var situacao_financeira_pessoa_juridica_id : Int? = null

    @OneToMany(mappedBy = "aprovacaoautomatica", cascade = [CascadeType.ALL], orphanRemoval = true)
    var produtos_de_credito : MutableList<AprovacaoAutomaticaProdutosCredito>? = null
}