package br.com.softfocus.credifocus.mvp.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "cadastros_supervisor")
class CadastrosSupervisor {

    @Id
    @Column(name = "id")
    var id : Int? = null

    @Column(name = "nome")
    var nome : String? = null
}