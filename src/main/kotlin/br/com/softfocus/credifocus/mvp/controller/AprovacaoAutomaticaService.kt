package br.com.softfocus.credifocus.mvp.controller

import br.com.softfocus.credifocus.mvp.interfaces.ResultadoModelViewSet
import br.com.softfocus.credifocus.mvp.model.AprovacaoAutomatica
import br.com.softfocus.credifocus.mvp.repository.AprovacaoAutomaticaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.websocket.server.PathParam


@RestController
@RequestMapping("/api/credito/")
class AprovacaoAutomaticaService {

    @Autowired
    lateinit var appRepo : AprovacaoAutomaticaRepository

    @CrossOrigin
    @GetMapping("aprovacao-automatica")
    fun listModelViewSet(@RequestParam(value = "page", defaultValue = "0") page : Int,
                         @RequestParam(value = "page_size", defaultValue = "3") size : Int): ResponseEntity<*> {

        val paging: Pageable = PageRequest.of(page, size)
        val res = appRepo.findAll(paging)
        val retorno = ResultadoModelViewSet()
        retorno.count = res.totalElements?.toInt()
        retorno.results = res.content as MutableList<Any>?
        return ResponseEntity.ok(retorno)
    }

    @CrossOrigin
    @PutMapping("atualiza-ativo")
    fun atualizaAtivo(@RequestParam(value = "id", defaultValue = "0") id : Int, @RequestParam(value = "ativo", defaultValue = "0") ativo : Boolean): ResponseEntity<*> {
        val aprovacaoAutomatica : AprovacaoAutomatica? = this.appRepo.findByIdOrNull(id.toLong())

        return if (aprovacaoAutomatica == null) {
            ResponseEntity.of(Optional.of(ResponseEntity.badRequest()))
        } else {
            aprovacaoAutomatica.ativo = ativo
            this.appRepo.save(aprovacaoAutomatica!!)
            ResponseEntity.ok(aprovacaoAutomatica)
        }
    }

    @CrossOrigin
    @PutMapping("atualiza-passo-1")
    fun atualizaPassoUm(@PathVariable(value = "aprovacaoAutomatica") aprovacaoAutomaticaParam: AprovacaoAutomatica): ResponseEntity<*> {
        val result = aprovacaoAutomaticaParam.id?.let {
            val res = this.appRepo.findByIdOrNull(it.toLong())
            val msg = validacoesAprovacaoAutomatica(aprovacaoAutomaticaParam)

            if (res == null) {
                ResponseEntity.notFound()
            } else if (!msg.isNullOrEmpty()) {
                ResponseEntity.of(Optional.of(msg))
            } else {
                res.nome = aprovacaoAutomaticaParam.nome
                res.ativo = aprovacaoAutomaticaParam.ativo
                res.inicio_vigencia = aprovacaoAutomaticaParam.inicio_vigencia
                res.final_vigencia = aprovacaoAutomaticaParam.final_vigencia
                res.valor_maximo = aprovacaoAutomaticaParam.valor_maximo
                res.score_risco_minimo = aprovacaoAutomaticaParam.score_risco_minimo
                res.nivel_risco_maximo = aprovacaoAutomaticaParam.nivel_risco_maximo
                res.permite_pessoa_fisica = aprovacaoAutomaticaParam.permite_pessoa_fisica
                res.permite_pessoa_juridica = aprovacaoAutomaticaParam.permite_pessoa_juridica
                res.produtos_de_credito = aprovacaoAutomaticaParam.produtos_de_credito

                this.appRepo.save(res)
                ResponseEntity.ok(res)
            }
        }
        /*
        aprovacao_automatica = get_object_or_404(AprovacaoAutomatica, id=request.data.get('id'))
        produtos_credito = []

        for item_produto in request.data.get('produtos_de_credito'):
            produtos_credito.append(ProdutoCredito.objects.get(id=item_produto))

        aprovacao_automatica.nome = request.data.get('nome')
        aprovacao_automatica.ativo = request.data.get('ativo')
        aprovacao_automatica.inicio_vigencia = request.data.get('inicio_vigencia')[0:10] if request.data.get('inicio_vigencia') else None
        aprovacao_automatica.final_vigencia = request.data.get('final_vigencia')[0:10] if request.data.get('final_vigencia') else None
        aprovacao_automatica.valor_maximo = request.data.get('valor_maximo')
        aprovacao_automatica.score_risco_minimo = request.data.get('score_risco_minimo')
        aprovacao_automatica.nivel_risco_maximo = request.data.get('nivel_risco_maximo')
        aprovacao_automatica.permite_pessoa_fisica = request.data.get('permite_pessoa_fisica')
        aprovacao_automatica.permite_pessoa_juridica = request.data.get('permite_pessoa_juridica')

        mensagem = self.validacoes_aprovacao_automatica(request.data.get('ativo'), produtos_credito, aprovacao_automatica)

        if mensagem:
            return Response(
                mensagem,
                status=status.HTTP_400_BAD_REQUEST
            )

        aprovacao_automatica.save()
        aprovacao_automatica.produtos_de_credito.clear()

        for item_produto in produtos_credito:
            aprovacao_automatica.produtos_de_credito.add(item_produto)

        return Response(
            AprovacaoAutomaticaSerializer(aprovacao_automatica).data,
            status=status.HTTP_200_OK
        )



         */
        return result as ResponseEntity<*>
    }

    @CrossOrigin
    @PutMapping("atualiza-passo-2")
    fun atualizaPassoDois(@PathVariable(value = "aprovacaoAutomatica") aprovacaoAutomaticaParam: AprovacaoAutomatica): ResponseEntity<*> {
        val aprovacaoAutomatica : AprovacaoAutomatica? = aprovacaoAutomaticaParam.id?.let {
            this.appRepo.findByIdOrNull(aprovacaoAutomaticaParam.id!!.toLong())
        }
        return ResponseEntity.ok(aprovacaoAutomatica?:"")
    }

    @CrossOrigin
    @PutMapping("atualiza-passo-3")
    fun atualizaPassoTres(@PathVariable(value = "aprovacaoAutomatica") aprovacaoAutomaticaParam: AprovacaoAutomatica): ResponseEntity<*> {
        val aprovacaoAutomatica : AprovacaoAutomatica? = aprovacaoAutomaticaParam.id?.let {
            this.appRepo.findByIdOrNull(aprovacaoAutomaticaParam.id!!.toLong())
        }
        return ResponseEntity.ok(aprovacaoAutomatica?:"")
    }

    @CrossOrigin
    @PutMapping("atualiza-passo-4")
    fun atualizaPassoQuatro(@PathVariable(value = "aprovacaoAutomatica") aprovacaoAutomaticaParam: AprovacaoAutomatica): ResponseEntity<*> {
        val aprovacaoAutomatica : AprovacaoAutomatica? = aprovacaoAutomaticaParam.id?.let {
            this.appRepo.findByIdOrNull(aprovacaoAutomaticaParam.id!!.toLong())
        }
        return ResponseEntity.ok(aprovacaoAutomatica?:"")
    }

    @CrossOrigin
    @PutMapping("atualiza-passo-5")
    fun atualizaPassoCinco(@PathVariable(value = "aprovacaoAutomatica") aprovacaoAutomaticaParam: AprovacaoAutomatica): ResponseEntity<*> {
        val aprovacaoAutomatica : AprovacaoAutomatica? = aprovacaoAutomaticaParam.id?.let {
            this.appRepo.findByIdOrNull(aprovacaoAutomaticaParam.id!!.toLong())
        }
        return ResponseEntity.ok(aprovacaoAutomatica?:"")
    }

    @CrossOrigin
    @PutMapping("atualiza-passo-6")
    fun atualizaPassoSeis(@PathVariable(value = "aprovacaoAutomatica") aprovacaoAutomaticaParam: AprovacaoAutomatica): ResponseEntity<*> {
        val aprovacaoAutomatica : AprovacaoAutomatica? = aprovacaoAutomaticaParam.id?.let {
            this.appRepo.findByIdOrNull(aprovacaoAutomaticaParam.id!!.toLong())
        }
        return ResponseEntity.ok(aprovacaoAutomatica?:"")
    }

    fun validacoesAprovacaoAutomatica(aprovacaoAutomatica : AprovacaoAutomatica): String? {

        if (aprovacaoAutomatica.maximo_cooperativa!! < aprovacaoAutomatica.maximo_produto_credito!!)
            return "O valor Máximo para cooperativa não pode ser inferior ao Máximo para Produtos."
        if (aprovacaoAutomatica.maximo_cooperativa!! < aprovacaoAutomatica.maximo_beneficiario!!)
            return "O valor Máximo para cooperativa não pode ser inferior ao Máximo para Beneficiários."
        if (aprovacaoAutomatica.maximo_produto_credito!! < aprovacaoAutomatica.maximo_beneficiario!!)
            return "O valor Máximo por Produto de Crédito não pode ser inferior ao Máximo para Beneficiários."
        return null
    }
}